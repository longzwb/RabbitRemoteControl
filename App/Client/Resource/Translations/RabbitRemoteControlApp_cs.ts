<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>CFavoriteView</name>
    <message>
        <location filename="../../FavoriteView.cpp" line="241"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="242"/>
        <source>Open settings and connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="246"/>
        <location filename="../../FavoriteView.cpp" line="250"/>
        <source>New group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="244"/>
        <location filename="../../FavoriteView.cpp" line="247"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="29"/>
        <source>Favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="285"/>
        <source>Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="285"/>
        <source>Input group name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="290"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="290"/>
        <source>The group [%1] is existed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CFrmFullScreenToolBar</name>
    <message>
        <location filename="../../FrmFullScreenToolBar.ui" line="16"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="36"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="40"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="41"/>
        <source>Nail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="44"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="45"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="46"/>
        <source>Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="50"/>
        <source>Zoom to windows</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="53"/>
        <source>Origin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="56"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="68"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="72"/>
        <source>ScreenShot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="74"/>
        <source>Add to favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="76"/>
        <source>TabBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="79"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="80"/>
        <source>Tab bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="82"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="83"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="84"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="87"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="88"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="89"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CFrmListConnects</name>
    <message>
        <location filename="../../FrmListConnects.cpp" line="23"/>
        <source>List connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="26"/>
        <location filename="../../FrmListConnects.cpp" line="361"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="28"/>
        <location filename="../../FrmListConnects.cpp" line="362"/>
        <source>Edit and Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="36"/>
        <location filename="../../FrmListConnects.cpp" line="40"/>
        <location filename="../../FrmListConnects.cpp" line="41"/>
        <location filename="../../FrmListConnects.cpp" line="42"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="45"/>
        <location filename="../../FrmListConnects.cpp" line="365"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="47"/>
        <location filename="../../FrmListConnects.cpp" line="366"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="49"/>
        <location filename="../../FrmListConnects.cpp" line="367"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="56"/>
        <location filename="../../FrmListConnects.cpp" line="369"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="53"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="79"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="80"/>
        <source>Protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="81"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="82"/>
        <source>ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="84"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="209"/>
        <location filename="../../FrmListConnects.cpp" line="303"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="210"/>
        <source>File of connecter is exists. whether to overwrite it? File: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="304"/>
        <source>File of connecter is exists. whether to overwrite it? If select No, please modify the name of connecter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CParameterDlgSettings</name>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="14"/>
        <location filename="../../ParameterDlgSettings.ui" line="55"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="66"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="218"/>
        <source>Shot screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="245"/>
        <source>No action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="261"/>
        <source>Open folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="274"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="74"/>
        <source>Recent open file max count:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="334"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="96"/>
        <source>Favorite: select it then double node edit, other connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="117"/>
        <source>Main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="211"/>
        <source>Main window receiver short cut key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="295"/>
        <source>Browser(&amp;B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="307"/>
        <source>Save main window status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="143"/>
        <source>Tab position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="168"/>
        <source>North</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="181"/>
        <source>South</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="194"/>
        <source>West</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="155"/>
        <source>East</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="285"/>
        <source>Save Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="314"/>
        <source>System tray icon context menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="327"/>
        <source>Main menu bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="320"/>
        <source>Remote</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="341"/>
        <source>Recent open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="230"/>
        <source>Select shot remote desktop, otherwise shot window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="204"/>
        <source>Resume the connections when it was last closed at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="123"/>
        <source>Enable system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="39"/>
        <source>Ok(&amp;O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="46"/>
        <source>No(&amp;N)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.cpp" line="164"/>
        <source>Open shot screen path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Rabbit Remote Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="32"/>
        <source>Help(&amp;H)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="39"/>
        <source>View(&amp;V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="43"/>
        <location filename="../../mainwindow.cpp" line="175"/>
        <location filename="../../mainwindow.cpp" line="176"/>
        <location filename="../../mainwindow.cpp" line="177"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="67"/>
        <source>Remote(&amp;R)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="71"/>
        <source>Connect(&amp;C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="92"/>
        <source>Tools(&amp;T)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="108"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="137"/>
        <source>About(&amp;A)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="140"/>
        <location filename="../../mainwindow.ui" line="143"/>
        <location filename="../../mainwindow.ui" line="146"/>
        <location filename="../../mainwindow.ui" line="149"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="161"/>
        <location filename="../../mainwindow.ui" line="164"/>
        <location filename="../../mainwindow.ui" line="167"/>
        <location filename="../../mainwindow.ui" line="170"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="184"/>
        <location filename="../../mainwindow.ui" line="187"/>
        <location filename="../../mainwindow.ui" line="190"/>
        <location filename="../../mainwindow.ui" line="193"/>
        <source>ToolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="232"/>
        <location filename="../../mainwindow.ui" line="235"/>
        <location filename="../../mainwindow.ui" line="238"/>
        <location filename="../../mainwindow.ui" line="241"/>
        <source>Original size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="277"/>
        <source>Keep aspect ration zoom to window(&amp;K)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="280"/>
        <location filename="../../mainwindow.ui" line="283"/>
        <location filename="../../mainwindow.ui" line="286"/>
        <location filename="../../mainwindow.ui" line="289"/>
        <source>Keep aspect ration zoom to window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="304"/>
        <location filename="../../mainwindow.ui" line="307"/>
        <location filename="../../mainwindow.ui" line="310"/>
        <location filename="../../mainwindow.ui" line="313"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="325"/>
        <location filename="../../mainwindow.ui" line="328"/>
        <location filename="../../mainwindow.ui" line="331"/>
        <location filename="../../mainwindow.ui" line="334"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="346"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="375"/>
        <source>TabBar(&amp;B)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="378"/>
        <location filename="../../mainwindow.ui" line="381"/>
        <location filename="../../mainwindow.ui" line="384"/>
        <location filename="../../mainwindow.ui" line="387"/>
        <source>TabBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="540"/>
        <location filename="../../mainwindow.ui" line="543"/>
        <location filename="../../mainwindow.ui" line="546"/>
        <location filename="../../mainwindow.ui" line="549"/>
        <location filename="../../mainwindow.ui" line="552"/>
        <source>Open the list of connections(&amp;O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="564"/>
        <source>Main menu bar(&amp;M)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="567"/>
        <location filename="../../mainwindow.ui" line="570"/>
        <location filename="../../mainwindow.ui" line="573"/>
        <location filename="../../mainwindow.ui" line="576"/>
        <source>Main menu bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="587"/>
        <location filename="../../mainwindow.ui" line="590"/>
        <location filename="../../mainwindow.ui" line="593"/>
        <location filename="../../mainwindow.ui" line="596"/>
        <source>Status bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="584"/>
        <source>Status bar(&amp;S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="158"/>
        <source>Update(&amp;U)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="181"/>
        <source>ToolBar(&amp;T)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="202"/>
        <location filename="../../mainwindow.cpp" line="366"/>
        <source>Full screen(&amp;F)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="205"/>
        <location filename="../../mainwindow.ui" line="208"/>
        <location filename="../../mainwindow.ui" line="211"/>
        <location filename="../../mainwindow.ui" line="214"/>
        <location filename="../../mainwindow.cpp" line="367"/>
        <location filename="../../mainwindow.cpp" line="368"/>
        <location filename="../../mainwindow.cpp" line="369"/>
        <source>Full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="229"/>
        <source>Original size(&amp;O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="253"/>
        <source>Zoom to window(&amp;Z)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="256"/>
        <location filename="../../mainwindow.ui" line="259"/>
        <location filename="../../mainwindow.ui" line="262"/>
        <location filename="../../mainwindow.ui" line="265"/>
        <source>Zoom to window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="301"/>
        <source>Exit(&amp;E)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="110"/>
        <source>Recently connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="322"/>
        <source>Disconnect(&amp;D)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="343"/>
        <source>Open(&amp;O) rabbit remote control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="360"/>
        <source>Send Ctl+Alt+Del</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="399"/>
        <location filename="../../mainwindow.ui" line="402"/>
        <location filename="../../mainwindow.ui" line="405"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="417"/>
        <location filename="../../mainwindow.ui" line="420"/>
        <location filename="../../mainwindow.ui" line="423"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="447"/>
        <location filename="../../mainwindow.ui" line="450"/>
        <location filename="../../mainwindow.ui" line="453"/>
        <location filename="../../mainwindow.ui" line="456"/>
        <source>Screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="465"/>
        <location filename="../../mainwindow.ui" line="468"/>
        <location filename="../../mainwindow.ui" line="471"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="480"/>
        <location filename="../../mainwindow.ui" line="483"/>
        <location filename="../../mainwindow.ui" line="486"/>
        <location filename="../../mainwindow.ui" line="489"/>
        <source>Current connect parameters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="498"/>
        <location filename="../../mainwindow.ui" line="501"/>
        <location filename="../../mainwindow.ui" line="504"/>
        <source>Clone current connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="513"/>
        <location filename="../../mainwindow.ui" line="516"/>
        <location filename="../../mainwindow.ui" line="519"/>
        <location filename="../../mainwindow.ui" line="522"/>
        <source>Add to favorite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="531"/>
        <source>Style(&amp;S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="432"/>
        <location filename="../../mainwindow.ui" line="435"/>
        <location filename="../../mainwindow.ui" line="438"/>
        <source>Zoom window to remote desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="165"/>
        <location filename="../../mainwindow.cpp" line="166"/>
        <location filename="../../mainwindow.cpp" line="167"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="394"/>
        <source>Exit full screen(&amp;E)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="395"/>
        <location filename="../../mainwindow.cpp" line="396"/>
        <location filename="../../mainwindow.cpp" line="397"/>
        <source>Exit full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="602"/>
        <location filename="../../mainwindow.cpp" line="620"/>
        <source>Load file fail: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="349"/>
        <location filename="../../mainwindow.ui" line="352"/>
        <location filename="../../mainwindow.ui" line="355"/>
        <location filename="../../mainwindow.cpp" line="612"/>
        <source>Open rabbit remote control file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="240"/>
        <source>ICE signal status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="303"/>
        <source>### Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="614"/>
        <source>Rabbit remote control Files (*.rrc);;All files(*.*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="702"/>
        <source>Connecting to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="728"/>
        <source>Connected to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="755"/>
        <source>ICE signal status: Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="764"/>
        <source>ICE signal status: Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="935"/>
        <source>Hide menu bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="936"/>
        <source>The menu bar will be hidden, the tool bar must be showed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="964"/>
        <source>Hide tool bar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="965"/>
        <source>The tool bar will be hidden, the menu bar must be showed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="993"/>
        <source>Save screenslot to </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="73"/>
        <source>Rabbit Remote Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="74"/>
        <source>Kang Lin Studio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
