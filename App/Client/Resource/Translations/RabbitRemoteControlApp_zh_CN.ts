<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CFavoriteView</name>
    <message>
        <location filename="../../FavoriteView.cpp" line="241"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="242"/>
        <source>Open settings and connect</source>
        <translation>打开设置再连接</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="246"/>
        <location filename="../../FavoriteView.cpp" line="250"/>
        <source>New group</source>
        <translation>新建组</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="244"/>
        <location filename="../../FavoriteView.cpp" line="247"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="29"/>
        <source>Favorite</source>
        <translation>收藏夹</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="285"/>
        <source>Input</source>
        <translation>输入</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="285"/>
        <source>Input group name</source>
        <translation>输入组名</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="290"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../FavoriteView.cpp" line="290"/>
        <source>The group [%1] is existed</source>
        <translation>组 [%1] 已经存在</translation>
    </message>
</context>
<context>
    <name>CFrmFullScreenToolBar</name>
    <message>
        <location filename="../../FrmFullScreenToolBar.ui" line="16"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="36"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="40"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="41"/>
        <source>Nail</source>
        <translation>钉住</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="44"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="45"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="46"/>
        <source>Full</source>
        <translation>关闭全屏</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="50"/>
        <source>Zoom to windows</source>
        <translation>远程桌面缩放到客户端窗口大小</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="53"/>
        <source>Origin</source>
        <translation>远程桌面还原到原始大小</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="56"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="68"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="72"/>
        <source>ScreenShot</source>
        <translation>截屏</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="74"/>
        <source>Add to favorite</source>
        <translation>增加到收藏夹</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="87"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="88"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="89"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="82"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="83"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="84"/>
        <source>Disconnect</source>
        <translation>关闭连接</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="76"/>
        <source>TabBar</source>
        <translation>标签条</translation>
    </message>
    <message>
        <location filename="../../FrmFullScreenToolBar.cpp" line="79"/>
        <location filename="../../FrmFullScreenToolBar.cpp" line="80"/>
        <source>Tab bar</source>
        <translation>标签条</translation>
    </message>
</context>
<context>
    <name>CFrmListConnects</name>
    <message>
        <location filename="../../FrmListConnects.cpp" line="23"/>
        <source>List connections</source>
        <translation>连接列表</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="26"/>
        <location filename="../../FrmListConnects.cpp" line="361"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="28"/>
        <location filename="../../FrmListConnects.cpp" line="362"/>
        <source>Edit and Connect</source>
        <translation>编辑并连接</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="36"/>
        <location filename="../../FrmListConnects.cpp" line="40"/>
        <location filename="../../FrmListConnects.cpp" line="41"/>
        <location filename="../../FrmListConnects.cpp" line="42"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="45"/>
        <location filename="../../FrmListConnects.cpp" line="365"/>
        <source>Edit</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="47"/>
        <location filename="../../FrmListConnects.cpp" line="366"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="49"/>
        <location filename="../../FrmListConnects.cpp" line="367"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="56"/>
        <location filename="../../FrmListConnects.cpp" line="369"/>
        <source>Refresh</source>
        <translation>刷新</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="53"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="79"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="80"/>
        <source>Protocol</source>
        <translation>协议</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="81"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="82"/>
        <source>ID</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="84"/>
        <source>File</source>
        <translation>文件</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="209"/>
        <location filename="../../FrmListConnects.cpp" line="303"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="210"/>
        <source>File of connecter is exists. whether to overwrite it? File: %1</source>
        <translation>此连接文件已存在,是否覆盖它？文件: %1</translation>
    </message>
    <message>
        <location filename="../../FrmListConnects.cpp" line="304"/>
        <source>File of connecter is exists. whether to overwrite it? If select No, please modify the name of connecter</source>
        <translation>此连接文件已存在,是否覆盖它？如果选择否，请修改连接的名称。</translation>
    </message>
</context>
<context>
    <name>CParameterDlgSettings</name>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="14"/>
        <location filename="../../ParameterDlgSettings.ui" line="55"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="66"/>
        <source>General</source>
        <translation>普通</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="218"/>
        <source>Shot screen</source>
        <translation>截屏</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="245"/>
        <source>No action</source>
        <translation>无动作</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="261"/>
        <source>Open folder</source>
        <translation>打开文件夹</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="274"/>
        <source>Open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="74"/>
        <source>Recent open file max count:</source>
        <translation>最近打开文件最大数：</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="334"/>
        <source>No</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="96"/>
        <source>Favorite: select it then double node edit, other connect</source>
        <translation>收藏夹：选择它，双击编辑节点；否则连接</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="117"/>
        <source>Main window</source>
        <translation>主窗口</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="211"/>
        <source>Main window receiver short cut key</source>
        <translation>主窗口接收快捷键</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="295"/>
        <source>Browser(&amp;B)</source>
        <translation>浏览(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="307"/>
        <source>Save main window status</source>
        <translation>保存主窗口状态</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="143"/>
        <source>Tab position</source>
        <translation>标签位置</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="168"/>
        <source>North</source>
        <translation>上</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="181"/>
        <source>South</source>
        <translation>下</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="194"/>
        <source>West</source>
        <translation>左</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="155"/>
        <source>East</source>
        <translation>右</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="285"/>
        <source>Save Path:</source>
        <translation>保存文件夹：</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="314"/>
        <source>System tray icon context menu</source>
        <translation>系统托盘图标右键菜单</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="327"/>
        <source>Main menu bar</source>
        <translation>主菜单条</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="320"/>
        <source>Remote</source>
        <translation>远程菜单</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="341"/>
        <source>Recent open</source>
        <translation>最近打开菜单</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="230"/>
        <source>Select shot remote desktop, otherwise shot window</source>
        <translation>选择截取远程桌面，否则截取窗口</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="204"/>
        <source>Resume the connections when it was last closed at startup</source>
        <translation>启动时，恢复上次关闭时的连接</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="123"/>
        <source>Enable system tray icon</source>
        <translation>允许系统托盘图标</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="39"/>
        <source>Ok(&amp;O)</source>
        <translation>确定(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.ui" line="46"/>
        <source>No(&amp;N)</source>
        <translation>取消(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../ParameterDlgSettings.cpp" line="164"/>
        <source>Open shot screen path</source>
        <translation>打开截屏文件夹</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Rabbit Remote Control</source>
        <translation>玉兔远程控制</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="32"/>
        <source>Help(&amp;H)</source>
        <translation>帮助(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="39"/>
        <source>View(&amp;V)</source>
        <translation>视图(&amp;V)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="43"/>
        <location filename="../../mainwindow.cpp" line="175"/>
        <location filename="../../mainwindow.cpp" line="176"/>
        <location filename="../../mainwindow.cpp" line="177"/>
        <source>Zoom</source>
        <translation>缩放</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="67"/>
        <source>Remote(&amp;R)</source>
        <translation>远程(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="92"/>
        <source>Tools(&amp;T)</source>
        <translation>工具(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="108"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="137"/>
        <source>About(&amp;A)</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="158"/>
        <source>Update(&amp;U)</source>
        <translation>更新(&amp;U)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="181"/>
        <source>ToolBar(&amp;T)</source>
        <translation>工具条(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="202"/>
        <location filename="../../mainwindow.cpp" line="366"/>
        <source>Full screen(&amp;F)</source>
        <translation>全屏(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="253"/>
        <source>Zoom to window(&amp;Z)</source>
        <translation>远程桌面缩放到窗口大小(&amp;Z)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="256"/>
        <location filename="../../mainwindow.ui" line="259"/>
        <location filename="../../mainwindow.ui" line="262"/>
        <location filename="../../mainwindow.ui" line="265"/>
        <source>Zoom to window</source>
        <translation>远程桌面缩放到窗口大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="304"/>
        <location filename="../../mainwindow.ui" line="307"/>
        <location filename="../../mainwindow.ui" line="310"/>
        <location filename="../../mainwindow.ui" line="313"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="325"/>
        <location filename="../../mainwindow.ui" line="328"/>
        <location filename="../../mainwindow.ui" line="331"/>
        <location filename="../../mainwindow.ui" line="334"/>
        <source>Disconnect</source>
        <translation>关闭边连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="346"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="375"/>
        <source>TabBar(&amp;B)</source>
        <translation>标签条(&amp;B)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="378"/>
        <location filename="../../mainwindow.ui" line="381"/>
        <location filename="../../mainwindow.ui" line="384"/>
        <location filename="../../mainwindow.ui" line="387"/>
        <source>TabBar</source>
        <translation>标签条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="399"/>
        <location filename="../../mainwindow.ui" line="402"/>
        <location filename="../../mainwindow.ui" line="405"/>
        <source>Zoom In</source>
        <translation>放大</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="417"/>
        <location filename="../../mainwindow.ui" line="420"/>
        <location filename="../../mainwindow.ui" line="423"/>
        <source>Zoom Out</source>
        <translation>缩小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="447"/>
        <location filename="../../mainwindow.ui" line="450"/>
        <location filename="../../mainwindow.ui" line="453"/>
        <location filename="../../mainwindow.ui" line="456"/>
        <source>Screenshot</source>
        <translation>截屏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="465"/>
        <location filename="../../mainwindow.ui" line="468"/>
        <location filename="../../mainwindow.ui" line="471"/>
        <source>Settings</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="480"/>
        <location filename="../../mainwindow.ui" line="483"/>
        <location filename="../../mainwindow.ui" line="486"/>
        <location filename="../../mainwindow.ui" line="489"/>
        <source>Current connect parameters</source>
        <translation>当前连接参数</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="498"/>
        <location filename="../../mainwindow.ui" line="501"/>
        <location filename="../../mainwindow.ui" line="504"/>
        <source>Clone current connect</source>
        <translation>克隆当前连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="513"/>
        <location filename="../../mainwindow.ui" line="516"/>
        <location filename="../../mainwindow.ui" line="519"/>
        <location filename="../../mainwindow.ui" line="522"/>
        <source>Add to favorite</source>
        <translation>增加到收藏夹</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="531"/>
        <source>Style(&amp;S)</source>
        <translation>样式(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="540"/>
        <location filename="../../mainwindow.ui" line="543"/>
        <location filename="../../mainwindow.ui" line="546"/>
        <location filename="../../mainwindow.ui" line="549"/>
        <location filename="../../mainwindow.ui" line="552"/>
        <source>Open the list of connections(&amp;O)</source>
        <translation>打开连接列表(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="564"/>
        <source>Main menu bar(&amp;M)</source>
        <translation>主菜单条(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="567"/>
        <location filename="../../mainwindow.ui" line="570"/>
        <location filename="../../mainwindow.ui" line="573"/>
        <location filename="../../mainwindow.ui" line="576"/>
        <source>Main menu bar</source>
        <translation>主菜单条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="587"/>
        <location filename="../../mainwindow.ui" line="590"/>
        <location filename="../../mainwindow.ui" line="593"/>
        <location filename="../../mainwindow.ui" line="596"/>
        <source>Status bar</source>
        <translation>状态条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="584"/>
        <source>Status bar(&amp;S)</source>
        <translation>状态条(&amp;S)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="432"/>
        <location filename="../../mainwindow.ui" line="435"/>
        <location filename="../../mainwindow.ui" line="438"/>
        <source>Zoom window to remote desktop</source>
        <translation>缩放窗口到远程桌面大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="140"/>
        <location filename="../../mainwindow.ui" line="143"/>
        <location filename="../../mainwindow.ui" line="146"/>
        <location filename="../../mainwindow.ui" line="149"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="161"/>
        <location filename="../../mainwindow.ui" line="164"/>
        <location filename="../../mainwindow.ui" line="167"/>
        <location filename="../../mainwindow.ui" line="170"/>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="184"/>
        <location filename="../../mainwindow.ui" line="187"/>
        <location filename="../../mainwindow.ui" line="190"/>
        <location filename="../../mainwindow.ui" line="193"/>
        <source>ToolBar</source>
        <translation>工具条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="229"/>
        <source>Original size(&amp;O)</source>
        <translation>远程桌面还原到原始大小(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="232"/>
        <location filename="../../mainwindow.ui" line="235"/>
        <location filename="../../mainwindow.ui" line="238"/>
        <location filename="../../mainwindow.ui" line="241"/>
        <source>Original size</source>
        <translation>原始大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="277"/>
        <source>Keep aspect ration zoom to window(&amp;K)</source>
        <translation>远程桌面保持纵横比缩放到窗口大小(&amp;K)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="280"/>
        <location filename="../../mainwindow.ui" line="283"/>
        <location filename="../../mainwindow.ui" line="286"/>
        <location filename="../../mainwindow.ui" line="289"/>
        <source>Keep aspect ration zoom to window</source>
        <translation>远程桌面保持纵横比缩放到窗口大小</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="301"/>
        <source>Exit(&amp;E)</source>
        <translation>退出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="343"/>
        <source>Open(&amp;O) rabbit remote control file</source>
        <translation>打开玉兔远程控制文件(&amp;O)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="360"/>
        <source>Send Ctl+Alt+Del</source>
        <translation>发送 Ctl+Alt+Del</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="71"/>
        <source>Connect(&amp;C)</source>
        <translation>连接(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="110"/>
        <source>Recently connected</source>
        <translation>最近连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="322"/>
        <source>Disconnect(&amp;D)</source>
        <translation>断开连接(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="165"/>
        <location filename="../../mainwindow.cpp" line="166"/>
        <location filename="../../mainwindow.cpp" line="167"/>
        <source>Connect</source>
        <translation>连接</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="205"/>
        <location filename="../../mainwindow.ui" line="208"/>
        <location filename="../../mainwindow.ui" line="211"/>
        <location filename="../../mainwindow.ui" line="214"/>
        <location filename="../../mainwindow.cpp" line="367"/>
        <location filename="../../mainwindow.cpp" line="368"/>
        <location filename="../../mainwindow.cpp" line="369"/>
        <source>Full screen</source>
        <translation>全屏</translation>
    </message>
    <message>
        <source>ICE singal status</source>
        <translation type="vanished">ICE 信令状态</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="394"/>
        <source>Exit full screen(&amp;E)</source>
        <translation>退出全屏(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="395"/>
        <location filename="../../mainwindow.cpp" line="396"/>
        <location filename="../../mainwindow.cpp" line="397"/>
        <source>Exit full screen</source>
        <translation>退出全屏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="602"/>
        <location filename="../../mainwindow.cpp" line="620"/>
        <source>Load file fail: </source>
        <translation>加载文件失败：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="349"/>
        <location filename="../../mainwindow.ui" line="352"/>
        <location filename="../../mainwindow.ui" line="355"/>
        <location filename="../../mainwindow.cpp" line="612"/>
        <source>Open rabbit remote control file</source>
        <translation>打开玉兔远程控制文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="240"/>
        <source>ICE signal status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="303"/>
        <source>### Plugin</source>
        <translation>### 插件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="614"/>
        <source>Rabbit remote control Files (*.rrc);;All files(*.*)</source>
        <translation>玉兔远程控制文件(*.rrc);;所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="702"/>
        <source>Connecting to </source>
        <translation>正在连接 </translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="728"/>
        <source>Connected to </source>
        <translation>连接到 </translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="755"/>
        <source>ICE signal status: Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="764"/>
        <source>ICE signal status: Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="935"/>
        <source>Hide menu bar</source>
        <translation>隐藏菜单栏</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="936"/>
        <source>The menu bar will be hidden, the tool bar must be showed.</source>
        <translation>将隐藏菜单栏，必须显示工具条。</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="964"/>
        <source>Hide tool bar</source>
        <translation>隐藏工具条</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="965"/>
        <source>The tool bar will be hidden, the menu bar must be showed.</source>
        <translation>将隐藏工具条，必须显示菜单栏。</translation>
    </message>
    <message>
        <source>ICE singal status: Connected</source>
        <translation type="vanished">ICE 信令状态：连接</translation>
    </message>
    <message>
        <source>ICE singal status: Disconnected</source>
        <translation type="vanished">ICE 信令状态：断开</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="993"/>
        <source>Save screenslot to </source>
        <translation>保存截屏到 </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="73"/>
        <source>Rabbit Remote Control</source>
        <translation>玉兔远程控制</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="74"/>
        <source>Kang Lin Studio</source>
        <translation>康林工作室</translation>
    </message>
</context>
</TS>
